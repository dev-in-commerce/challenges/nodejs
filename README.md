# Desafio Labi9 Backend - NodeJs


## Criar uma API Node de carrinho com autenticação e rotas protegidas

Desafio técnico criado para medir seus conhecimentos e habilidades de programação backend [Node](https://nodejs.org).

Recomendamos a utilização de bibliotecas para a documentação dos endpoints


## Endpoints

Usuarios:

- Registrar

- Login

- Logout

Produtos (Apenas Usuários Logados) :

- Listar produtos

- Cadastrar produto

- Alterar produto

- Mostrar um produto

- Deletar produto

- Adicionar produto ao carrinho do usuário

- Remover produto ao carrinho do usuário

- Finalizar a Compra


## Algumas Models

User: 

- Nome *

- Email *

- Senha *


Produtos:

- Nome *

- Preço *

- Quantidade *

- Descrição


\* Campos com (*) são obrigatorios

\*\* As demais models ficam a seu criterio os seus nomes e campos

## O que será avaliado:

- Qualidade e simplicidade do código.

- Utilização do padrão RESTful.

- Utilização dos padrões ECMA mais recentes.

- Autenticação JWT ou outra de sua preferencia.

- Documentação dos endpoints

- Lógica do carrinho de compras

- Utilização do Typescript.


## Diferencial

- Utilização de Patterns

- Tests cobrindo todos os pontos

- Utilização do inglês no codigo


## Aplicação

A aplicação é basicamente um carrinho onde o usuario poderá se registrar. Ao logar ele podera cadastrar produtos, listar, alterar e deleta-los.
O usuario tbm poderá vincular produtos ao seu carrinho e finalizar a compra.


## Entrega

A aplicação deve ser criada em modo privado no seu repositorio github ou gitlab,
ao termino será necessario enviar um email para comercial@labi9.com junto dele enviar a collection do postman ou insomia e adicionar @jcsfran (caso utilizar github  adicionar [jcsfran](https://github.com/jcsfran)) ao repositorio.
